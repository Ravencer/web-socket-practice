import classes from './App.module.scss'
import Canvas from './components/Canvas'
import SettingBar from './components/SettingBar'
import Toolbar from './components/Toolbar'

function App() {

  return (
    <div className={classes.app}>
      <Toolbar/>
      <SettingBar/>
      <Canvas/>
    </div>
  )
}

export default App
