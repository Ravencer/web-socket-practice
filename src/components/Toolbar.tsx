import canvasState from '../store/canvasState'
import toolState from '../store/toolState'
import Brush from '../tools/Brush'
import Circle from '../tools/Circle'
import Rect from '../tools/Rect'
import classes from './SettingBar.module.scss'
// type Props = {}

const Toolbar = () => {
  return (
    <div className={classes.settingBar}>
      <button className={classes.settingBar_btn + ' ' + classes.brush} 
      onClick={() => {
        if (canvasState.canvas) {
          toolState.setTool(new Brush(canvasState.canvas, 1, 1));
        }
        }}>
      </button>

      <button className={classes.settingBar_btn + ' ' + classes.rect}
      onClick={() => {
        if (canvasState.canvas) {
          toolState.setTool(new Rect(canvasState.canvas, 1, 1));
        }
      }}>
      </button>

      <button className={classes.settingBar_btn + ' ' + classes.circle}
      onClick={() => {
        if (canvasState.canvas) {
          toolState.setTool(new Circle(canvasState.canvas, 1, 1));
        }
      }}>
      </button>
      <button className={classes.settingBar_btn + ' ' + classes.eraser}></button>
      <button className={classes.settingBar_btn + ' ' + classes.line}></button>
      <input type='color' style={{marginLeft:'10px'}}></input>
      <button className={classes.settingBar_btn + ' ' + classes.undo}></button>
      <button className={classes.settingBar_btn + ' ' + classes.redo}></button>
      <button className={classes.settingBar_btn + ' ' + classes.save}></button>
    </div>
  )
}

export default Toolbar