import { useEffect, useRef } from 'react'
import classes from './Canvas.module.scss'
import { observer } from 'mobx-react-lite';
import canvasState from '../store/canvasState';
import toolState from '../store/toolState';
import Brush from '../tools/Brush';



const Canvas = observer(() => {
  const canvasRef = useRef<HTMLCanvasElement>(null);


  useEffect(() => {
    canvasState.setCanvas(canvasRef.current);
    toolState.setTool(new Brush(canvasRef.current as HTMLCanvasElement, 1, 3,))
  }, [])

  return (
    <div className={classes.canvas}>
        <canvas ref={canvasRef} width={600} height={400}>

        </canvas>
    </div>
  )
});

export default Canvas