import Tool from "./tool";

export default class Circle extends Tool{
    mouseDown: boolean = false;
    mouseUp: boolean = false;
    mouseMove: boolean = false;
    startX: number = 0;
    startY: number = 0;
    saved: string = '';
    constructor(canvas : HTMLCanvasElement, socket : number, id: number) {
        super(canvas, socket, id);
        
        this.listen();
    }

    listen(){
        this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
        this.canvas.onmousedown = this.mouseDownHandler.bind(this);
        this.canvas.onmouseup = this.mouseUpHandler.bind(this);
    }

    mouseUpHandler(){
        this.mouseDown = false;
    }   

    mouseDownHandler(e: MouseEvent){
        this.mouseDown = true;
        this.startX = e.pageX - (e.target as HTMLElement).offsetLeft;
        this.startY = e.pageY - (e.target as HTMLElement).offsetTop;
        this.ctx.beginPath();
        this.saved = this.canvas.toDataURL();
    }

    mouseMoveHandler(e: MouseEvent){
        if(this.mouseDown){
            const currentX = e.pageX - (e.target as HTMLElement).offsetLeft;
            const currentY = e.pageY - (e.target as HTMLElement).offsetTop;
            const width = currentX - this.startX;
            const height = currentY - this.startY;
            const r = Math.sqrt(width**2 + height**2);
            this.draw(this.startX, this.startY, r);
        }
    }

    draw(x: number, y: number, r: number){
        const img = new Image();
        img.src = this.saved;
        img.onload = async () => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
            this.ctx.beginPath();
            this.ctx.arc(x, y, r, 0, 2*Math.PI);
            this.ctx.fill();
            this.ctx.stroke();
        }
    }
}