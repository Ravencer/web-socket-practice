import { makeAutoObservable } from "mobx";
import Tool from "../tools/tool";

class ToolState {
    tool = null;
    constructor(){
        makeAutoObservable(this)
    }

    setTool(tool : Tool | null){
        this.tool = tool as null;
    }
}

export default new ToolState();