import { makeAutoObservable } from "mobx";

class CanvasState {
    canvas = null;
    constructor(){
        makeAutoObservable(this)
    }

    setCanvas(canvas : CanvasRenderingContext2D | null){
        this.canvas = canvas as null;
    }
}

export default new CanvasState();